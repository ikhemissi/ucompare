'use strict';

const test = require('ava');
const { throwIfEmptyString, throwIfNotPositiveNumber } = require('../../lib/validation');

test('validate strings', (t) => {
  t.notThrows(() => throwIfEmptyString('sdfasdfsdfs'));
  t.throws(() => throwIfEmptyString(''), Error);
});

test('validate numbers', (t) => {
  t.notThrows(() => throwIfNotPositiveNumber(1234));
  t.notThrows(() => throwIfNotPositiveNumber(0));
  t.notThrows(() => throwIfNotPositiveNumber('1234'));
  t.throws(() => throwIfNotPositiveNumber('asd'), Error);
  t.throws(() => throwIfNotPositiveNumber(-1), Error);
});
