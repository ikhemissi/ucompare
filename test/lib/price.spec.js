'use strict';

const test = require('ava');
const { price } = require('../../lib');

test('calculate energy cost based on annual usage estimation', (t) => {
  const plan = {
    supplier: 'InfinityStones',
    plan: 'Tesseract',
    rates: [
      { price: 10, threshold: 90 },
      { price: 5 },
    ],
  };

  const { cost } = price.planCost(100, plan);

  // 950 * 1.05 = 997.5 pence = 9.975 pound
  t.is(cost, 9.98);
});

test('compare energy cost between all plans', (t) => {
  const plans = [
    {
      supplier: 'InfinityStones',
      plan: 'Tesseract',
      rates: [
        { price: 10, threshold: 90 },
        { price: 5 },
      ],
    },
    {
      supplier: 'Dr. Gero',
      plan: 'Android18',
      rates: [
        { price: 18, threshold: 42 },
        { price: 6 },
      ],
    },
  ];

  const pricing = price.comparePlans(100, plans);

  t.deepEqual(pricing, [
    {
      supplier: 'InfinityStones',
      plan: 'Tesseract',
      cost: 9.98,
    },
    {
      supplier: 'Dr. Gero',
      plan: 'Android18',
      cost: 11.59,
    },
  ]);
});
