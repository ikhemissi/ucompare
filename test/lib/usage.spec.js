'use strict';

const test = require('ava');
const { usage } = require('../../lib');

test('estimate energy amount from a spend and a list of rates', (t) => {
  const rates = [
    { price: 10, threshold: 90 },
    { price: 5 },
  ];

  const energy = usage.estimateEnergyAmount(1000, rates);

  t.is(energy, 100);
});

test('estimate energy amount from a spend, a list of rates, and a standing charge', (t) => {
  const standingCharge = 1;
  const rates = [
    { price: 10, threshold: 90 },
    { price: 5 },
  ];

  // 1 * 365 * 1.05 = 383.25
  const energy = usage.estimateEnergyAmount(1383.25, rates, standingCharge);

  t.is(energy, 100);
});
