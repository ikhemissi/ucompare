'use strict';

const fs = require('fs');
const path = require('path');
const test = require('ava');
const childProcess = require('child-process-promise');

test.serial('verify output using uSwitch input and expected_output data', (t) => {
  const inputs = fs.readFileSync(path.join(__dirname, './fixtures/inputs'), 'utf8');
  const expectedOutput = fs.readFileSync(path.join(__dirname, './fixtures/expected_output'), 'utf8');
  const cliPath = path.join(__dirname, '../cli.js');
  const plansPath = path.join(__dirname, './fixtures/plans.json');

  const command = childProcess.spawn(cliPath, [plansPath], { capture: ['stdout', 'stderr'] });

  command.progress((child) => {
    child.stdin.setEncoding('utf-8');
    child.stdin.end(inputs);
  });

  return command
    .then((result) => {
      const output = result.stdout.toString();
      t.is(output, expectedOutput);
    });
});

test.serial('return an error when receiving an unknown command', (t) => {
  const cliPath = path.join(__dirname, '../cli.js');
  const plansPath = path.join(__dirname, './fixtures/plans.json');

  const command = childProcess.spawn(cliPath, [plansPath], { capture: ['stdout', 'stderr'] });

  command.progress((child) => {
    child.stdin.setEncoding('utf-8');
    child.stdin.end('advertize');
  });

  return command
    .catch((error) => {
      t.is(error.code, 1);
      t.is(error.stderr, 'Unsupported operation advertize\n');
    });
});

test.serial('return an error when a command execution throws an error', (t) => {
  const cliPath = path.join(__dirname, '../cli.js');
  const plansPath = path.join(__dirname, './fixtures/plans.json');

  const command = childProcess.spawn(cliPath, [plansPath], { capture: ['stdout', 'stderr'] });

  command.progress((child) => {
    child.stdin.setEncoding('utf-8');
    child.stdin.end('price somevalue');
  });

  return command
    .catch((error) => {
      t.is(error.code, 1);
      t.is(error.stderr, 'Energy "somevalue" must be a positive number\n');
    });
});
