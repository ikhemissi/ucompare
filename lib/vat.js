'use strict';

function plusVat(amount) {
  return Math.round(amount * 1.05); // VAT = 5%
}

function minusVat(amount) {
  return amount / 1.05; // VAT = 5%
}

module.exports = {
  minusVat,
  plusVat,
};
