'use strict';

function readCommands() {
  return new Promise((resolve) => {
    let input = '';

    process.stdin.setEncoding('utf8');

    process.stdin.on('readable', () => {
      const chunk = process.stdin.read();
      if (chunk !== null) {
        input += chunk;
      }
    });

    process.stdin.on('end', () => {
      const commands = input.split('\n')
        .map(line => line.split(' '))
        .map(tokens => ({ operation: tokens[0], parameters: tokens.slice(1) }));

      resolve(commands);
    });
  });
}

module.exports = {
  readCommands,
};
