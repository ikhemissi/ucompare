'use strict';

function throwIfEmptyString(value, name) {
  if (typeof value !== 'string' || !value.length) {
    throw new Error(`${name} must be a non-empty string`);
  }
}

function throwIfNotPositiveNumber(value, name) {
  const number = Number(value);

  if (Number.isNaN(number) || number < 0) {
    throw new Error(`${name} "${value}" must be a positive number`);
  }
}

module.exports = {
  throwIfEmptyString,
  throwIfNotPositiveNumber,
};
