'use strict';

const { plusVat } = require('./vat');
const { throwIfNotPositiveNumber } = require('./validation');

function planCost(annualEnergyUsage, planDetails) {
  let costInPence = planDetails.standing_charge ? planDetails.standing_charge * 365 : 0;
  let remainingEnergy = annualEnergyUsage;

  planDetails.rates.forEach(({ price, threshold }) => {
    const rateEnergyAllowance = (threshold === undefined || remainingEnergy < threshold) ? remainingEnergy : threshold;

    costInPence += rateEnergyAllowance * price;
    remainingEnergy -= rateEnergyAllowance;
  });

  const costAfterVat = plusVat(costInPence);
  const costInPounds = Number((costAfterVat / 100).toFixed(2));

  return {
    supplier: planDetails.supplier,
    plan: planDetails.plan,
    cost: costInPounds,
  };
}

function comparePlans(annualEnergyUsage, availablePlans = []) {
  const costs = availablePlans
    .map(plan => planCost(annualEnergyUsage, plan))
    .sort((a, b) => a.cost - b.cost);

  return costs;
}

function calculateAndPrintPrices(plans, annualEnergyUsage) {
  throwIfNotPositiveNumber(annualEnergyUsage, 'Energy');

  const energyUsage = Number(annualEnergyUsage);
  const planCosts = comparePlans(energyUsage, plans);

  planCosts.forEach(({ supplier, plan, cost }) => {
    console.log(`${supplier},${plan},${cost}`);
  });
}

module.exports = {
  planCost,
  comparePlans,
  calculateAndPrintPrices,
};
