'use strict';

const { minusVat } = require('./vat');
const { throwIfEmptyString, throwIfNotPositiveNumber } = require('./validation');

function estimateEnergyAmount(spendInPence, rates, standingCharge = 0) {
  const spendAfterVat = minusVat(spendInPence);
  const standingCharges = standingCharge * 365; // In pence

  if (spendAfterVat < standingCharges) {
    // throw 'The spend cannot cover standing charges';
    return 0;
  }

  const nbRates = rates.length;
  let energyTotal = 0;
  let remainingMoney = spendAfterVat - standingCharges;

  for (let rateIndex = 0; rateIndex < nbRates && remainingMoney > 0; rateIndex++) {
    const rate = rates[rateIndex];
    const maxEnergyWithThisRate = remainingMoney / rate.price;
    const purchased = rate.threshold && rate.threshold < maxEnergyWithThisRate ? rate.threshold : maxEnergyWithThisRate;

    energyTotal += purchased;
    remainingMoney -= purchased * rate.price;
  }

  return Math.round(energyTotal);
}

function calculateAndPrintUsage(plans, supplierName, planName, monthlySpendInPound) {
  throwIfEmptyString(supplierName, 'Supplier');
  throwIfEmptyString(planName, 'Plan');
  throwIfNotPositiveNumber(monthlySpendInPound, 'Spend');

  const planDetails = plans.find(({ supplier, plan }) => supplierName === supplier && planName === plan);

  if (!planDetails) {
    throw new Error(`No data is available for the plan ${planName} of the supplier ${supplierName}`);
  }

  const monthlySpendInPence = Number(monthlySpendInPound) * 100;
  const yearlySpend = monthlySpendInPence * 12;
  const energy = estimateEnergyAmount(yearlySpend, planDetails.rates, planDetails.standing_charge);

  console.log(energy);
}

module.exports = {
  estimateEnergyAmount,
  calculateAndPrintUsage,
};
