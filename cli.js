#!/usr/bin/env node

'use strict';

const { readCommands } = require('./lib/commands');
const { calculateAndPrintPrices } = require('./lib/price');
const { calculateAndPrintUsage } = require('./lib/usage');

const plansFile = process.argv[2];
let plans = null;

try {
  // Fetch the list of plans from the supplied path
  // plansFile must point to a JSON file (readable using 'require')
  plans = require(plansFile);
} catch (_) {
  console.error(`Could not read plans from ${plansFile}`);
  process.exit(1);
}

// Exit program without errors
function exit() {
  process.exit(0);
}

const handlers = {
  exit,
  price: calculateAndPrintPrices,
  usage: calculateAndPrintUsage,
};

readCommands()
  .then((commands) => {
    commands.forEach(({ operation, parameters }) => {
      const handler = operation && handlers[operation];

      if (!handler) {
        throw new Error(`Unsupported operation ${operation}`);
      }

      handler(plans, ...parameters); // Call operation handler and pass it the list of plans, and all extra parameters
    });
  })
  .catch((error) => {
    console.error(error.message);
    process.exit(1);
  });
