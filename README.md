# uCompare

> Energy plans comparator

## Description
This package (if published/linked) can be used in 2 ways: as a cli and as a library.

### cli command
```sh
ucompare plans.json < commands
```

### library:
```javascript
const { usage } = require('ucompare');

const energy = usage.estimateEnergyAmount(spendInPence, planRates, standingCharge);
```

## Dependencies
uCompare does no require any dependencies for runtime (whether it is used in cli or library mode).
This being said, you will need to install the development dependencies if you want to modify it.

To do that, please make sure you follow these steps:
- Ensure you have a [Node.js](https://nodejs.org/en/) installed in your machine using `node -v`
- If you do not have a [Node.js](https://nodejs.org/en/) 6 or higher, please install [nvm](https://github.com/creationix/nvm) and run the commands `nvm install` and `nvm use` at the root of the project
- Now, you can run `npm install` at the root of the project to install all the dependencies

## Installing this package without registry
To install this package locally (without publishing it to a registry like [npm](https://www.npmjs.com/)), you need to run the following command from the root of the project: `npm link`.
This will make the package available globally and you can use it everywhere as a library or CLI command (you may need to run `npm link ucompare` to use it as a library).
